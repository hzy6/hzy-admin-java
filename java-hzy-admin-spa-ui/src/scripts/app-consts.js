/**
 * 程序常量
 */
const appConsts = {
    /**
     * token 键值 用于存储浏览器内存中
     */
    tokenKey: "Authorization_HZY_ADMIN_For_Java",
    /**
     * 域名
     */
    domainName: process.env.NODE_ENV == "production" ? "" : "http://localhost:6789",
    /**
     * Guid Empty String
     */
    guidEmpty: '00000000-0000-0000-0000-000000000000',
    /**
     * app 前缀 用于浏览器本地缓存 key 的前缀
     */
    appPrefix: "Java-HzyAdminAntdVue",
    /**
     * 用于请求头部授权码key名称
     */
    authorizationKeyName: "Authorization_HZY_ADMIN_For_Java",
};

export default appConsts;