import { post } from '@/scripts/request';

const controllerName = "admin/gen";

export default {
    /**
    * 查询列表
    * 
    * @param {一页显示多少行} rows 
    * @param {当前页码} page 
    */
    findList(rows, page, search = {}) {
        return post(`${controllerName}/findList/${rows}/${page}`, search, false);
    },

    /**
     * 获取domain
     * @returns 
     */
    genDomain(form) {
        return post(`${controllerName}/domain`, form, false);
    },

    /**
     * 获取mapper
     */
    genMapper(form) {
        return post(`${controllerName}/mapper`, form, false);
    },

    /**
     * 获取 mapperXml
     */
    genMapperXml(form) {
        return post(`${controllerName}/mapperXml`, form, false);
    },

    /**
     * 获取 service
     */
    genService(form) {
        return post(`${controllerName}/service`, form, false);
    },

    /**
     * 获取 service impl
     */
    genServiceImpl(form) {
        return post(`${controllerName}/serviceImpl`, form, false);
    },

    /**
     * 获取 controller
     */
    genController(form) {
        return post(`${controllerName}/controller`, form, false);
    },

    /**
     * 获取 service Js
     */
    genServiceJs(form) {
        return post(`${controllerName}/serviceJs`, form, false);
    },

    /**
     * 获取 index vue
     */
    genIndexVue(form) {
        return post(`${controllerName}/indexVue`, form, false);
    },

    /**
     * 获取 info vue
     */
    genInfoVue(form) {
        return post(`${controllerName}/infoVue`, form, false);
    },




};