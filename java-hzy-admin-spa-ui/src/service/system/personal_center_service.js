import { post } from '@/scripts/request';

const controllerName = "admin/system/personalCenter";

export default {
    /**
     * 修改密码
     *
     * @param {表单数据} form
     */
    changePassword(form) {
        return post(`${controllerName}/changePassword`, form);
    },
    saveForm(form) {
        return post(`${controllerName}/updateCurrentUser`, form);
    }
};