package com.hzy.infrastructure.exception;

import com.hzy.infrastructure.exception.CustomException;

/**
 * <p>
 * MessageBox 消息框
 * </p>
 *
 * @author hzy
 * @since 2020-04-26
 */
public class MessageBox {

    public static void show() throws CustomException {
        throw new CustomException();
    }

    public static void show(String message) throws CustomException {
        throw new CustomException(message, null);
    }

    public static <T> void showData(T data) throws CustomException {
        throw new CustomException(data);
    }

    public static <T> void show(String message, T data) throws CustomException {
        throw new CustomException(message, data);
    }

}
