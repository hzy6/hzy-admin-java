package com.hzy.infrastructure.exception;

/**
 * <p>
 * ApiResult Ajax 返回 对象
 * </p>
 *
 * @author HZY
 * @since 2020-04-26
 */
public class ApiResult<T> {

    public ApiResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResult(StatusCodeEnum statusCodeEnum, String message, T data) {
        this.code = statusCodeEnum.getValue();
        this.message = message;
        this.data = data;
    }

    private int code;
    private String message;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    /**
     * ApiResult status 的枚举值
     */
    public enum StatusCodeEnum {
        /**
         * 接口不存在
         */
        NotFount(-3),
        /**
         * 程序异常
         */
        Error(-2),
        /**
         * 未授权
         */
        UnAuth(-1),
        /**
         * 警告
         */
        Warn(0),
        /**
         * 成功
         */
        Ok(1);

        StatusCodeEnum(int value) {
            this.value = value;
        }

        private final int value;

        public int getValue() {
            return this.value;
        }
    }

    /**
     * 返回
     *
     * @param code
     * @param message
     * @return ApiResult
     */
    public static ApiResult<?> resultMessage(int code, String message) {
        return new ApiResult<Object>(code, message, null);
    }

    /**
     * 返回
     *
     * @param code
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultData(int code, T data) {
        return new ApiResult<T>(code, "result", data);
    }

    /**
     * 返回
     *
     * @param code
     * @param message
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> result(int code, String message, T data) {
        return new ApiResult<T>(code, message, data);
    }

    /**
     * 返回
     *
     * @param code
     * @param message
     * @return ApiResult
     */
    public static ApiResult<?> resultMessage(StatusCodeEnum code, String message) {
        return new ApiResult<Object>(code, message, null);
    }

    /**
     * 返回
     *
     * @param code
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultData(StatusCodeEnum code, T data) {
        return new ApiResult<T>(code, "result", data);
    }

    /**
     * 返回
     *
     * @param code
     * @param message
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> result(StatusCodeEnum code, String message, T data) {
        return new ApiResult<T>(code, message, data);
    }

    /**
     * 成功
     *
     * @return ApiResult
     */
    public static ApiResult<?> resultOk() {
        return ApiResult.result(StatusCodeEnum.Ok, "success", null);
    }

    /**
     * 成功
     *
     * @param message
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultOk(String message, T data) {
        return ApiResult.result(StatusCodeEnum.Ok, message, data);
    }

    /**
     * 成功
     *
     * @param message 消息
     * @return ApiResult
     */
    public static ApiResult<?> resultMessageOk(String message) {
        return ApiResult.resultMessage(StatusCodeEnum.Ok, message);
    }

    /**
     * 成功
     *
     * @param data 数据
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultDataOk(T data) {
        return ApiResult.result(StatusCodeEnum.Ok, "success", data);
    }

    /**
     * 警告
     *
     * @return ApiResult
     */
    public static ApiResult<?> resultWarn() {
        return ApiResult.resultMessage(StatusCodeEnum.Warn, "warn");
    }

    /**
     * 警告
     *
     * @param message
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultWarn(String message, T data) {
        return ApiResult.result(StatusCodeEnum.Warn, message, data);
    }

    /**
     * 警告
     *
     * @param message
     * @return ApiResult
     */
    public static ApiResult<?> resultMessageWarn(String message) {
        return ApiResult.resultMessage(StatusCodeEnum.Warn, message);
    }

    /**
     * 警告
     *
     * @param data
     * @return ApiResult
     */
    public static <T> ApiResult<T> resultDataWarn(T data) {
        return ApiResult.result(StatusCodeEnum.Warn, "warn", data);
    }


}
