package com.hzy.infrastructure.exception;

/**
 * <p>
 * 自定义异常
 * </p>
 *
 * @author hzy
 * @since 2020-04-26
 */
public class CustomException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private final ApiResult<?> apiResult;

    public CustomException() {
        super("warn");
        this.apiResult = ApiResult.resultWarn();
    }

    public CustomException(String msg) {
        super(msg);
        this.apiResult = ApiResult.resultMessageWarn(msg);
    }

    public <T> CustomException(T data) {
        super("warn");
        this.apiResult = ApiResult.resultDataWarn(data);
    }

    public <T> CustomException(String msg, T data) {
        super(msg);
        this.apiResult = ApiResult.resultWarn(msg, data);
    }

    /**
     * 获取 ApiResult
     *
     * @return
     */
    public ApiResult<?> getApiResult() {
        return apiResult;
    }
}
