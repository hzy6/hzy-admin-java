package com.hzy.infrastructure.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * SystemAuthority 用于描述接口权限码
 * </p>
 *
 * @author hzy
 * @since 2020-04-26
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited()
// @Documented
public @interface AdminActionDescribe {

    /**
     * 权限功能码
     *
     * @return
     */
    String value() default "display";

    /**
     * action 描述
     *
     * @return
     */
    String actionRemark() default "action 描述!";

}
