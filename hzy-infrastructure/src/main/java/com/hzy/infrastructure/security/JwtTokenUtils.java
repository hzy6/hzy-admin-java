package com.hzy.infrastructure.security;

import com.alibaba.fastjson.JSON;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * jwt 生成工具
 * </p>
 * <p>
 * https://www.cnblogs.com/yueguanguanyun/p/9055049.html
 * https://www.cnblogs.com/shihaiming/p/9565835.html
 *
 * @author hzy
 * @since 2020-04-26
 */
public class JwtTokenUtils {

    private static final String SECRET = "XX#$%()(#*!()!KL<><MQLMNQNQJQK sdfkjsdrow32234545fdf>?N<:{LWPW";

    private static final String EXP = "exp";

    private static final String PAYLOAD = "payload";

    /**
     * 加密，传入一个对象和有效期
     *
     * @param object
     * @param maxAge
     * @param <T>
     * @return
     */
    public static <T> String sign(T object, long maxAge) {
        try {
            final JWTSigner signer = new JWTSigner(SECRET);
            final Map<String, Object> claims = new HashMap<String, Object>();
            String jsonString = JSON.toJSONString(object);
            claims.put(PAYLOAD, jsonString);
            claims.put(EXP, System.currentTimeMillis() + maxAge);
            return signer.sign(claims);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 解密，传入一个加密后的token字符串和解密后的类型
     *
     * @param jwt
     * @param classT
     * @param <T>
     * @return
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws JWTVerifyException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public static <T> T unSign(String jwt, Class<T> classT)
            throws SignatureException, NoSuchAlgorithmException, JWTVerifyException, InvalidKeyException, IOException {
        final JWTVerifier verifier = new JWTVerifier(SECRET);

        final Map<String, Object> claims = verifier.verify(jwt);
        if (claims.containsKey(EXP) && claims.containsKey(PAYLOAD)) {
            long exp = (Long) claims.get(EXP);
            long currentTimeMillis = System.currentTimeMillis();
            if (exp > currentTimeMillis) {
                return JSON.parseObject((String) claims.get(PAYLOAD), classT);
            }
        }
        return null;
    }

}
