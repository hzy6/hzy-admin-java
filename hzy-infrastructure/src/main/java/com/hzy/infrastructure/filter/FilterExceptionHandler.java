package com.hzy.infrastructure.filter;

import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 异常 过滤器
 * </p>
 *
 * @author HZY
 * @since 2020-04-26
 */
@ControllerAdvice
public class FilterExceptionHandler {
    Logger logger = LoggerFactory.getLogger(FilterExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(value = CustomException.class)
    public ApiResult<?> messageBoxHandler(CustomException e) {
        return e.getApiResult();
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ApiResult<?> exceptionHandler(Exception e) {
        String errMessage = e.getMessage();
        e.printStackTrace();
        logger.error("============== 程序异常 ============");
        logger.error(errMessage, e);
        return ApiResult.resultMessage(ApiResult.StatusCodeEnum.Error, errMessage);
    }

}
