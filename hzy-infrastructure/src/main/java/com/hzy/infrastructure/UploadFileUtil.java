package com.hzy.infrastructure;

import org.springframework.util.ObjectUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * 上传文件工具类
 *
 * @author HZY
 */
public class UploadFileUtil {
    private static String resourcePath;
    private static String savePath;

    /**
     * 初始化信息
     *
     * @throws FileNotFoundException
     */
    private static void init() throws FileNotFoundException {

        if (ObjectUtils.isEmpty(resourcePath)) {
            resourcePath = ResourceUtils.getURL("classpath:").getPath();
        }

        if (ObjectUtils.isEmpty(savePath)) {
            savePath = "system_upload";
        }

    }

    /**
     * 保存文件 在 static / system_upload /
     *
     * @param file
     * @return
     * @throws Exception
     */
    public static String save(final MultipartFile file) throws Exception {
        UploadFileUtil.init();
        return UploadFileUtil.save(file, UploadFileUtil.savePath);
    }

    /**
     * 保存多个文件 在 static / system_upload /
     *
     * @param files
     * @return
     * @throws Exception
     */
    public static ArrayList<String> save(final MultipartFile[] files) throws Exception {
        UploadFileUtil.init();
        return UploadFileUtil.save(files, UploadFileUtil.savePath);
    }

    /**
     * 保存文件
     *
     * @param file
     * @param folder 例如:"system_upload"
     * @return
     * @throws Exception
     */
    public static String save(final MultipartFile file, String folder) throws Exception {
        folder = "/" + folder + "/";
        if (file.isEmpty()) {
            throw new Exception("请选择要上传的文件");
        }

        if (ObjectUtils.isEmpty(resourcePath)) {
            UploadFileUtil.init();
        }

        final File uploadDir = new File(resourcePath, "/static" + folder);
        if (!uploadDir.isDirectory()) {
            uploadDir.mkdir();
        }

        final String time_ymd = LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        File time_ymd_file = new File(resourcePath, "/static" + folder + time_ymd);
        if (!time_ymd_file.isDirectory()) {
            time_ymd_file.mkdir();
        }

        folder += time_ymd + "/";

        String time = LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("HHmmss"));

        String fileName = file.getOriginalFilename();
        String newFileName = time + "_" + fileName;
        final File f = new File(time_ymd_file.getAbsolutePath(), newFileName);
        file.transferTo(f);

        return folder + newFileName;
    }

    /**
     * 保存文件
     *
     * @param files
     * @return
     * @throws Exception
     */
    public static ArrayList<String> save(final MultipartFile[] files, String folder) throws Exception {
        if (files == null) {
            throw new Exception("请选择要上传的文件");
        }

        if (files.length == 0) {
            throw new Exception("请选择要上传的文件");
        }

        if (ObjectUtils.isEmpty(resourcePath)) {
            UploadFileUtil.init();
        }

        final File uploadDir = new File(resourcePath, "/static" + folder);
        if (!uploadDir.isDirectory()) {
            uploadDir.mkdir();
        }

        ArrayList<String> paths = new ArrayList<>();
        for (MultipartFile multipartFile : files) {
            paths.add(UploadFileUtil.save(multipartFile, folder));
        }

        return paths;
    }

}
