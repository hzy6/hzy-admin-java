package com.hzy.infrastructure.domain.consts;

/**
 * 系统程序常量类
 *
 * @author hzy
 */
public class AppConst {
    /**
     * 基础管理路由前缀
     */
    public final static String ADMIN_ROUTE_PREFIX = "/api/admin/";

    /**
     * 系统路由前缀
     */
    public final static String SYSTEM_ROUTE_PREFIX = ADMIN_ROUTE_PREFIX + "system/";

    /**
     * 基础管理路由前缀
     */
    public final static String BASE_ROUTE_PREFIX = ADMIN_ROUTE_PREFIX + "base/";
}
