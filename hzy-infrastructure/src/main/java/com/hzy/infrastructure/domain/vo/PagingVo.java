package com.hzy.infrastructure.domain.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * 分页视图模型
 *
 * @author hzy
 */
public class PagingVo<T> {
    private long page;
    private long size;
    private long total;
    private List<T> dataSource;
    private IPage<T> pageObject;

    @JsonIgnore
    public IPage<T> getPageObject() {
        return pageObject;
    }

    public static <T> PagingVo<T> page(IPage<T> iPage, List<T> data) {
        PagingVo<T> tableVo = new PagingVo<>();
        tableVo.pageObject = iPage;
        tableVo.setPage(iPage.getCurrent());
        tableVo.setSize(iPage.getSize());
        tableVo.setTotal(iPage.getTotal());
        tableVo.setDataSource(data);
        return tableVo;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getDataSource() {
        return dataSource;
    }

    public void setDataSource(List<T> dataSource) {
        this.dataSource = dataSource;
    }

    public void setPageObject(IPage<T> iPage) {
        this.pageObject = iPage;
    }
}
