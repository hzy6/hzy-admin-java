package com.hzy.infrastructure.web.controller;

/**
 * 接口基础类
 *
 * @author HZY
 */
public class ApiBaseController<TService> extends BaseController {
    protected TService service;

    public ApiBaseController(TService service) {
        super();
        this.service = service;
    }


}
