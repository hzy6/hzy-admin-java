package com.hzy.infrastructure.web.controller;

import com.hzy.infrastructure.exception.ApiResult;

/**
 * 接口基础类
 *
 * @author hzy
 */
public class BaseController {

    /**
     * @param status
     * @param msg
     * @return
     */
    protected ApiResult<?> result(int status, String msg) {
        return ApiResult.resultMessage(status, msg);
    }

    /**
     * @param status
     * @param data
     * @return
     */
    protected <T> ApiResult<T> result(int status, T data) {
        return ApiResult.resultData(status, data);
    }

    /**
     * @param status
     * @param msg
     * @param data
     * @return
     */
    protected <T> ApiResult<T> result(int status, String msg, T data) {
        return ApiResult.result(status, msg, data);
    }

    /**
     * @param status
     * @param msg
     * @return
     */
    protected ApiResult<?> result(ApiResult.StatusCodeEnum status, String msg) {
        return ApiResult.resultMessage(status, msg);
    }

    /**
     * @param status
     * @param data
     * @return
     */
    protected <T> ApiResult<T> result(ApiResult.StatusCodeEnum status, T data) {
        return ApiResult.resultData(status, data);
    }

    /**
     * @param status
     * @param msg
     * @param data
     * @return
     */
    protected <T> ApiResult<T> result(ApiResult.StatusCodeEnum status, String msg, T data) {
        return ApiResult.result(status, msg, data);
    }

    /**
     * @return
     */
    protected ApiResult<?> resultOk() {
        return ApiResult.resultOk();
    }

    /**
     * @param msg
     * @return
     */
    protected ApiResult<?> resultMessageOk(String msg) {
        return ApiResult.resultMessageOk(msg);
    }

    /**
     * @param data
     * @return
     */
    protected <T> ApiResult<T> resultDataOk(T data) {
        return ApiResult.resultDataOk(data);
    }

    /**
     * @param msg
     * @param data
     * @return
     */
    protected <T> ApiResult<T> resultOk(String msg, T data) {
        return ApiResult.resultOk(msg, data);
    }

    /**
     * @return
     */
    protected ApiResult<?> resultWarn() {
        return ApiResult.resultWarn();
    }

    /**
     * @param msg
     * @return
     */
    protected ApiResult<?> resultMessageWarn(String msg) {
        return ApiResult.resultMessageWarn(msg);
    }

    /**
     * @param data
     * @return
     */
    protected <T> ApiResult<T> resultDataWarn(T data) {
        return ApiResult.resultDataWarn(data);
    }

    /**
     * @param msg
     * @param data
     * @return
     */
    protected <T> ApiResult<?> resultWarn(String msg, T data) {
        return ApiResult.resultWarn(msg, data);
    }


}
