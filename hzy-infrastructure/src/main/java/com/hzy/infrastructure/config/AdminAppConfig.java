package com.hzy.infrastructure.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 程序 配置信息
 *
 * @author HZY
 */
@Component
@ConfigurationProperties(prefix = "hzy-configs")
public class AdminAppConfig {

    /**
     * 名称
     */
    private String name;

    /**
     * 版本
     */
    private String version;

    /**
     * 页面标题
     */
    private String title;

    /**
     * token 键名
     */
    private String tokenKey;

    /**
     * 是否记录日志到数据库
     */
    private Boolean dbLog;

    /**
     * 超级管理员 角色 Id
     */
    private String adminRoleId;

    /**
     * 系统菜单Id
     */
    private Integer systemMenuId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public Boolean getDbLog() {
        return dbLog;
    }

    public void setDbLog(Boolean dbLog) {
        this.dbLog = dbLog;
    }

    public String getAdminRoleId() {
        return adminRoleId;
    }

    public void setAdminRoleId(String adminRoleId) {
        this.adminRoleId = adminRoleId;
    }

    public Integer getSystemMenuId() {
        return systemMenuId;
    }

    public void setSystemMenuId(Integer systemMenuId) {
        this.systemMenuId = systemMenuId;
    }
}
