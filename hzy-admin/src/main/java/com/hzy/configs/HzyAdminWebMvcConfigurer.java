package com.hzy.configs;

import com.hzy.filter.AdminAuthorizationFilterHandlerInterceptor;
import com.hzy.infrastructure.config.AdminAppConfig;
import com.hzy.system.service.IAccountService;
import com.hzy.system.service.ISysMenuService;
import com.hzy.system.service.ISysOperationLogService;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>
 * 请求 过滤器 配置
 * </p>
 *
 * @author HZY
 * @since 2020-04-26
 */
@Configuration
public class HzyAdminWebMvcConfigurer implements WebMvcConfigurer {

    private final AdminAppConfig adminAppConfig;
    private final ISysMenuService sysMenuService;
    private final IAccountService accountService;
    private final ISysOperationLogService sysOperationLogService;

    public HzyAdminWebMvcConfigurer(AdminAppConfig adminAppConfig, ISysMenuService sysMenuService, IAccountService accountService, ISysOperationLogService sysOperationLogService) {
        this.adminAppConfig = adminAppConfig;
        this.sysMenuService = sysMenuService;
        this.accountService = accountService;
        this.sysOperationLogService = sysOperationLogService;
    }

    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AdminAuthorizationFilterHandlerInterceptor(sysMenuService, adminAppConfig, accountService, sysOperationLogService)).addPathPatterns("/**");
    }

    /**
     * 解决跨域问题
     *
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOriginPatterns("*").allowedMethods("GET", "POST", "DELETE").allowCredentials(true).maxAge(3600).allowedHeaders("*");
    }
}
