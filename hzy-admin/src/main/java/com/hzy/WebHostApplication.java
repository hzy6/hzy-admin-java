package com.hzy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;

/**
 * 后台系统启动器
 *
 * @author HZY
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = { FreeMarkerAutoConfiguration.class })
public class WebHostApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebHostApplication.class, args);
	}

}
