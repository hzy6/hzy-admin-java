package com.hzy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.admin.domain.Member;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 会员
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface IMemberMapper extends BaseMapper<Member> {
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, Member search);
}
