package com.hzy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzy.system.domain.SysRole;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ITestMapper extends BaseMapper<SysRole> {
    SysRole get1();
}
