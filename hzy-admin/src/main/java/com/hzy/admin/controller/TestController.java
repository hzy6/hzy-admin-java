package com.hzy.admin.controller;

import com.hzy.admin.service.ITestService;
import com.hzy.system.domain.SysRole;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/")
public class TestController {
    private final ITestService _testService;

    public TestController(ITestService testService) {
        _testService = testService;
    }

    @ResponseBody
    @GetMapping("role")
    public SysRole GetUser() {
        return _testService.get();
    }
}
