package com.hzy.admin.controller;

import com.hzy.infrastructure.UploadFileUtil;
import com.hzy.infrastructure.web.controller.BaseController;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传控制器
 *
 * @author HZY
 */
@Api(tags = "文件上传控制器")
@RequestMapping("/upload")
@RestController
public class UploadController extends BaseController {

    /**
     * 上传文件
     *
     * @param file   文件对象
     * @param folder 文件存储分组文件夹
     * @return
     * @throws Exception
     */
    @ApiOperation("上传文件")
    @ResponseBody
    @PostMapping({"/single-file/{folder}", "/single-file"})
    public List<String> singleFile(@RequestParam("file") final MultipartFile file, @PathVariable String folder) throws Exception {
        List<String> result = new ArrayList<>();
        if (ObjectUtils.isEmpty(folder)) {
            result.add(UploadFileUtil.save(file));
        } else {
            result.add(UploadFileUtil.save(file, folder));
        }
        return result;
    }

}
