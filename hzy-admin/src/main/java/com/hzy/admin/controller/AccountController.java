package com.hzy.admin.controller;

import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.BaseController;
import com.hzy.system.domain.vo.AuthUserVo;
import com.hzy.system.service.IAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 登陆 授权
 *
 * @author HZY
 */
@Api(tags = "登陆 授权")
@RequestMapping(AppConst.ADMIN_ROUTE_PREFIX + "account/")
@RestController
public class AccountController extends BaseController {
    private final IAccountService accountService;

    public AccountController(IAccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 检查账户 获取token
     *
     * @param authUserVo 表单信息
     * @return json
     */
    @ApiOperation("检查账户")
    @PostMapping("check")
    @ResponseBody
    public ApiResult<?> check(@RequestBody AuthUserVo authUserVo) {
        String token = this.accountService.checkAccountAsync(authUserVo);
        Map<String, Object> map = new HashMap<>(1);
        map.put("token", token);
        return this.resultDataOk(map);
    }

}
