package com.hzy.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 跳转客户端控制器
 *
 * @author hzy
 */
@Controller
@RequestMapping("/")
public class JumpClientController {

    /**
     * 后台系统入口
     *
     * @return 跳转地址
     */
    @GetMapping
    public String index() {
        return "redirect:/client/index.html";
    }

    /**
     * 跳转接口文档 swagger 页面
     *
     * @return 跳转地址
     */
    @GetMapping("swagger")
    public String swagger() {
        return "redirect:/swagger-ui/index.html";
    }

    /**
     * 跳转阿里巴巴数据库连接池监控面板
     *
     * @return 跳转地址
     */
    @GetMapping("db")
    public String druid() {
        return "redirect:/druid";
    }

}
