package com.hzy.admin.controller;

import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.system.domain.dto.SysDictionaryTreeDto;
import com.hzy.system.service.ISysDictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公共接口控制器
 *
 * @author HZY
 */
@Api(tags = "公共接口控制器")
@RequestMapping(AppConst.ADMIN_ROUTE_PREFIX + "public/")
@RestController
public class PublicController {
    private final ISysDictionaryService sysDictionaryService;

    public PublicController(ISysDictionaryService sysDictionaryService) {

        this.sysDictionaryService = sysDictionaryService;
    }

    /**
     * 根据编码获取 字典集合
     *
     * @param code
     * @return
     */
    @ApiOperation("根据编码获取字典集合")
    @ResponseBody
    @GetMapping({"getDictionary/{code}"})
    public List<SysDictionaryTreeDto> GetDictionaryByCodeAsync(@PathVariable String code) {
        return this.sysDictionaryService.getDictionaryByCode(code);
    }


}
