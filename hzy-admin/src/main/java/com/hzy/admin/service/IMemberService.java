package com.hzy.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.admin.domain.Member;
import com.hzy.infrastructure.domain.vo.PagingVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface IMemberService extends IService<Member> {
    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, Member search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(String id);

    /**
     * 保存
     *
     * @param form
     * @param filePhoto
     * @param files
     * @return
     */
    String saveForm(Member form, final MultipartFile filePhoto, final MultipartFile[] files);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<String> ids);
}
