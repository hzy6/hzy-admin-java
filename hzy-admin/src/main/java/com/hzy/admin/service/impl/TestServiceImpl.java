package com.hzy.admin.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.admin.mapper.ITestMapper;
import com.hzy.admin.service.ITestService;
import com.hzy.system.domain.SysRole;
import org.springframework.stereotype.Service;

@Service
@DS("slave_1")
public class TestServiceImpl extends ServiceImpl<ITestMapper, SysRole> implements ITestService {

    @Override
    public SysRole get() {
        return this.baseMapper.get1();
    }
}
