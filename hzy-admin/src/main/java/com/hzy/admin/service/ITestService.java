package com.hzy.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.system.domain.SysRole;

public interface ITestService extends IService<SysRole> {
    public SysRole get();
}
