package com.hzy.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class Member extends AppBaseEntity<String> {

    private static final long serialVersionUID = 1L;

    private String number;

    private String name;

    private String phone;

    private String sex;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birthday;

    private String photo;

    @TableField("userId")
    private String userId;

    private String introduce;

    @TableField("filePath")
    private String filePath;

}
