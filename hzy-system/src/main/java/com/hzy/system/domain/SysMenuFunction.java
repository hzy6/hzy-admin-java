package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-29
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SysMenuFunction extends AppBaseEntity<String> {

    private static final long serialVersionUID = 1L;

    @TableField("menuId")
    private Integer menuId;

    @TableField("functionId")
    private String functionId;

}
