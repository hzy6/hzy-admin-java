package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SysFunction extends AppBaseEntity<String> {

    private static final long serialVersionUID = 1L;

    private Integer number;

    private String name;

    @TableField("byName")
    private String byName;

    private String remark;
}
