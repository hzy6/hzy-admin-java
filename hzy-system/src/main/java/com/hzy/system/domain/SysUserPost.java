package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户与岗位绑定
 *
 * @author hzy
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserPost extends AppBaseEntity<String> {

    private static final long serialVersionUID = 1L;

    @TableField("userId")
    private String userId;

    @TableField("postId")
    private String postId;
}
