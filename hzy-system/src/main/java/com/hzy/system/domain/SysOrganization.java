package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hzy.infrastructure.domain.AppBaseEntity;
import com.hzy.system.domain.enums.SysOrganizationStateEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 岗位
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysOrganization extends AppBaseEntity<Integer> {
    private static final long serialVersionUID = 1L;

    public SysOrganization() {
        this.state = 1;
    }

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 序号
     */
    @TableField("orderNumber")
    private Integer orderNumber;

    private String name;

    @TableField("levelCode")
    private String levelCode;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    private String email;

    /**
     * 1、正常 2、停用
     */
    private Integer state;

    @TableField("parentId")
    private Integer parentId;
}
