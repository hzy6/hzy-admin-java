package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRole extends AppBaseEntity<String> {
    public SysRole() {
        this.isDelete = true;
    }

    private static final long serialVersionUID = 1L;

    private Integer number;

    private String name;

    private String remark;

    /**
     * 能否删除
     */
    @TableField("isDelete")
    private Boolean isDelete;

}
