package com.hzy.system.domain.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HZY
 */
public class TableInfoTreeVo {
    public TableInfoTreeVo() {
        this.children = new ArrayList<>();
    }

    /**
     * 显示
     */
    private String label;
    /**
     * 注解
     */
    private String comment;
    /**
     * 等级
     */
    private Integer rank;
    /**
     * 数据类型
     */
    private String type;
    /**
     * 子集
     */
    private List<TableInfoTreeVo> children;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<TableInfoTreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<TableInfoTreeVo> children) {
		this.children = children;
	}
}
