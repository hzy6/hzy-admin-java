package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUser extends AppBaseEntity<String> {
    public SysUser() {
        this.isDelete = true;
    }

    private static final long serialVersionUID = 1L;

    private String name;

    @TableField("loginName")
    private String loginName;

    private String phone;

    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /**
     * 能否删除
     */
    @TableField("isDelete")
    private Boolean isDelete;

    @TableField("organizationId")
    private Integer organizationId;

}
