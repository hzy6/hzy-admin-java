package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenu extends AppBaseEntity<Integer> {

    public SysMenu() {
        this.show = true;
        this.close = true;
        this.keepAlive = true;
        this.state = true;
    }

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 等级码
     */
    @TableField("levelCode")
    private String levelCode;

    private Integer number;

    private String name;

    private String url;

    private String router;

    @TableField("componentName")
    private String componentName;

    @TableField("jumpUrl")
    private String jumpUrl;

    private String icon;

    @TableField("parentId")
    private Integer parentId;

    /**
     * 显示状态 => 显示 | 隐藏
     */
    @TableField("`show`")
    private Boolean show;

    /**
     * 选项卡是否可关闭
     */
    @TableField("`close`")
    private Boolean close;

    /**
     * 是否缓存 => 是 | 否
     */
    @TableField("keepAlive")
    private Boolean keepAlive;

    /**
     * 菜单状态 => 正常 | 停用
     */
    private Boolean state;

}
