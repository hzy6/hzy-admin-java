package com.hzy.system.domain.dto;

import com.hzy.system.domain.SysDictionary;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysDictionaryTreeDto extends SysDictionary {

    private Integer key;
    private String title;
    private List<SysDictionaryTreeDto> children;

}
