package com.hzy.system.domain.bo;

import com.hzy.system.domain.SysOrganization;
import com.hzy.system.domain.SysPost;
import com.hzy.system.domain.SysRole;
import com.hzy.system.domain.SysUser;
import com.hzy.system.domain.dto.SysMenuTreeDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * @author hzy
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AccountContext extends SysUser {

    /**
     * 程序标题
     */
    private String appTitle;

    /**
     * 角色 集合
     */
    private List<SysRole> sysRoles;

    /**
     * 角色ids
     */
    private List<String> roleIds;

    /**
     * 所属岗位
     */
    private List<SysPost> sysPosts;

    /**
     * 角色ids
     */
    private List<String> postIds;

    /**
     * 所属组织
     */
    private SysOrganization sysOrganization;

    /**
     * 是否超级管理员
     */
    private Boolean isAdministrator;

    /**
     * 菜单集合
     */
    private List<SysMenuTreeDto> menus;

    /**
     * 菜单功能集合
     */
    private List<Map<String, Object>> menuPowers;

}
