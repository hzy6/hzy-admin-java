package com.hzy.system.domain.vo;

import java.util.List;

/**
 * @author hzy
 */
public class SysRoleMenuFunctionVo {
    private String roleId;
    private Integer menuId;
    private List<String> functionIds;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public List<String> getFunctionIds() {
        return functionIds;
    }

    public void setFunctionIds(List<String> functionIds) {
        this.functionIds = functionIds;
    }

}
