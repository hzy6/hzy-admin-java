package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 岗位
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysDictionary extends AppBaseEntity<Integer> {
    private static final long serialVersionUID = 1L;

    public SysDictionary() {

    }

    private Integer sort;

    private String code;

    private String name;

    private String value;

    @TableField("parentId")
    private Integer parentId;
}
