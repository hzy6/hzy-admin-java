package com.hzy.system.domain.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 岗位状态
 * <p>
 *     1、正常
 * </p>
 * <p>
 *     2、停用
 * </p>
 * @author hzy
 */
public enum SysPostStateEnum {

    /**
     * 正常
     */
    NORMAL(1, "正常"),
    /**
     * 停用
     */
    STOP_USING(2, "停用");

    SysPostStateEnum(int code, String descp) {
        this.code = code;
        this.descp = descp;
    }

    /**
     * 标记数据库存的值是code
     */
    @EnumValue
    private final int code;

    private final String descp;


}