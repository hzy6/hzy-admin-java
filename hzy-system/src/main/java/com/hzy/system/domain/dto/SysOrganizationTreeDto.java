package com.hzy.system.domain.dto;

import com.hzy.system.domain.SysOrganization;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hzy
 */
public class SysOrganizationTreeDto extends SysOrganization {

    public SysOrganizationTreeDto() {
        this.children = new ArrayList<>();
    }

    private List<SysOrganizationTreeDto> children;

    public List<SysOrganizationTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<SysOrganizationTreeDto> children) {
        this.children = children;
    }
}
