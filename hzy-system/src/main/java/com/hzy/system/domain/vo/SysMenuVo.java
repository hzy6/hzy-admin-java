package com.hzy.system.domain.vo;

import com.hzy.system.domain.SysMenu;

import java.util.List;

public class SysMenuVo {
    private SysMenu form;
    private List<String> functionIds;
	public SysMenu getForm() {
		return form;
	}
	public void setForm(SysMenu form) {
		this.form = form;
	}
	public List<String> getFunctionIds() {
		return functionIds;
	}
	public void setFunctionIds(List<String> functionIds) {
		this.functionIds = functionIds;
	}
}
