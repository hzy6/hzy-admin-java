package com.hzy.system.domain.dto;

import com.hzy.system.domain.SysMenu;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hzy
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenuTreeDto extends SysMenu {
    public SysMenuTreeDto() {
        this.children = new ArrayList<>();
    }

    private String parentName;
    private List<SysMenuTreeDto> children;
}
