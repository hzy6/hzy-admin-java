package com.hzy.system.domain.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SysRoleMenuFunctionTreeVo {
    public SysRoleMenuFunctionTreeVo() {
        this.expandedRowKeys = new ArrayList<>();
        this.list = new ArrayList<>();
    }

    private List<String> expandedRowKeys;
    private List<Map<String, Object>> list;
	public List<String> getExpandedRowKeys() {
		return expandedRowKeys;
	}
	public void setExpandedRowKeys(List<String> expandedRowKeys) {
		this.expandedRowKeys = expandedRowKeys;
	}
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
}
