package com.hzy.system.domain.consts;

/**
 * 常量定义
 * 权限功能名称定义
 *
 * @author HZY
 */
public class AdminFunctionConst {
    /**
     * 功能常量 添加
     */
    public static final String INSERT = "Insert";
    /**
     * 功能常量 修改
     */
    public static final String UPDATE = "Update";
    /**
     * 功能常量 保存
     */
    public static final String SAVE = "Save";
    /**
     * 功能常量 删除
     */
    public static final String DELETE = "Delete";
    /**
     * 功能常量 是否拥有菜单
     */
    public static final String DISPLAY = "Display";
    /**
     * 功能常量 检索
     */
    public static final String SEARCH = "Search";
    /**
     * 功能常量 导出
     */
    public static final String EXPORT = "Export";
    /**
     * 功能常量 打印
     */
    public static final String PRINT = "Print";

}
