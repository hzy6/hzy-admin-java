package com.hzy.system.domain.vo;

import com.hzy.system.domain.SysRole;
import com.hzy.system.domain.SysUser;

import java.util.List;

/**
 * @author HZY
 */
public class UserVo extends BaseVo<SysUser> {
    private List<String> roleIds;
    private List<SysRole> allRoleList;
	public List<String> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}
	public List<SysRole> getAllRoleList() {
		return allRoleList;
	}
	public void setAllRoleList(List<SysRole> allRoleList) {
		this.allRoleList = allRoleList;
	}

}
