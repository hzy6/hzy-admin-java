package com.hzy.system.domain;

import com.hzy.infrastructure.domain.AppBaseEntity;
import com.hzy.system.domain.enums.SysPostStateEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 岗位
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysPost extends AppBaseEntity<String> {
    private static final long serialVersionUID = 1L;

    public SysPost() {
        this.state = 1;
    }

    /**
     * 序号
     */
    private Integer number;

    /**
     * 编码
     */
    private String code;

    private String name;

    /**
     * 启用状态 1、正常 2、停用
     */
    private Integer state;

    private String remark;
}


