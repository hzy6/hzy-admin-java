package com.hzy.system.domain.vo;

public class UpdatePasswordVo {
    private String oldPwd;
    private String newPwd;
    private String qrPwd;
	public String getOldPwd() {
		return oldPwd;
	}
	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}
	public String getNewPwd() {
		return newPwd;
	}
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}
	public String getQrPwd() {
		return qrPwd;
	}
	public void setQrPwd(String qrPwd) {
		this.qrPwd = qrPwd;
	}
}
