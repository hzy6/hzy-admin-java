package com.hzy.system.domain.vo;

import java.util.ArrayList;
import java.util.List;

public class TreeVo {
    public TreeVo() {
        this.disableCheckbox = false;
        this.disabled = false;
        this.children = new ArrayList<>();
    }

    private String key;
    private String title;
    private Boolean disableCheckbox;
    private Boolean disabled;
    private List<TreeVo> children;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean getDisableCheckbox() {
		return disableCheckbox;
	}
	public void setDisableCheckbox(Boolean disableCheckbox) {
		this.disableCheckbox = disableCheckbox;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public List<TreeVo> getChildren() {
		return children;
	}
	public void setChildren(List<TreeVo> children) {
		this.children = children;
	}
}
