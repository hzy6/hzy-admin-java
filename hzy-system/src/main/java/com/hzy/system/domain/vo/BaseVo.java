package com.hzy.system.domain.vo;

/**
 * @author HZY
 */
public class BaseVo<T> {
    private String id;
    private T model;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public T getModel() {
		return model;
	}
	public void setModel(T model) {
		this.model = model;
	}
}
