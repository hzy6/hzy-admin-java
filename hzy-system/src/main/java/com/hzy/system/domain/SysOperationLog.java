package com.hzy.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HZY
 * @since 2020-04-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysOperationLog extends AppBaseEntity<String> {

    private static final long serialVersionUID = 1L;

    private String api;

    private String ip;

    @TableField("userId")
    private String userId;

    private String form;

    @TableField("formBody")
    private String formBody;

    @TableField("queryString")
    private String queryString;

    @TableField("takeUpTime")
    private Long takeUpTime;

    @TableField("clientInfo")
    private String clientInfo;


}
