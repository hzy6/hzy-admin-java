package com.hzy.system.domain.vo;

import com.hzy.system.domain.SysPost;
import com.hzy.system.domain.SysRole;
import com.hzy.system.domain.SysUser;
import com.hzy.system.service.impl.SysPostServiceImpl;

import java.util.List;

/**
 * @author hzy
 */
public class SysUserVo {

    private SysUser form;
    private List<String> roleIds;
    private List<SysRole> allRoleList;
    private List<String> postIds;
    private List<SysPost> allPostList;

    public List<SysRole> getAllRoleList() {
        return allRoleList;
    }

    public void setAllRoleList(List<SysRole> allRoleList) {
        this.allRoleList = allRoleList;
    }

    public List<String> getPostIds() {
        return postIds;
    }

    public void setPostIds(List<String> postIds) {
        this.postIds = postIds;
    }

    public List<SysPost> getAllPostList() {
        return allPostList;
    }

    public void setAllPostList(List<SysPost> allPostList) {
        this.allPostList = allPostList;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    public SysUser getForm() {
        return form;
    }

    public void setForm(SysUser form) {
        this.form = form;
    }

}
