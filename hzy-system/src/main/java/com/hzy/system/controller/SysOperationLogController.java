package com.hzy.system.controller;

import com.hzy.infrastructure.annotation.AdminActionDescribe;
import com.hzy.infrastructure.annotation.AdminControllerDescriptor;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import com.hzy.system.domain.SysOperationLog;
import com.hzy.system.domain.consts.AdminFunctionConst;
import com.hzy.system.service.ISysOperationLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 操作日志控制器
 *
 * @author HZY
 */
@Api(tags = "操作日志控制器")
@RestController
@RequestMapping(AppConst.SYSTEM_ROUTE_PREFIX + "sysOperationLog/")
@AdminControllerDescriptor(24)
public class SysOperationLogController extends ApiBaseController<ISysOperationLogService> {

    public SysOperationLogController(ISysOperationLogService sysOperationLogService) {
        super(sysOperationLogService);
    }

    /**
     * 查询列表
     *
     * @param page   页码
     * @param size   页数
     * @param search 查询
     * @return json
     */
    @ApiOperation("查询列表")
    @PostMapping("findList/{size}/{page}")
    @ResponseBody
    @AdminActionDescribe(value = AdminFunctionConst.DISPLAY)
    public ApiResult<?> findList(@PathVariable Integer size, @PathVariable Integer page, @RequestBody SysOperationLog search) {
        PagingVo<Map<String, Object>> pagingVo = this.service.findList(page, size, search);
        return this.resultDataOk(pagingVo);
    }

    /**
     * 根据 id 查询表单信息
     *
     * @param id
     * @return json
     */
    @ApiOperation("根据 id 查询表单信息")
    @GetMapping({"findForm/{id}", "findForm"})
    @ResponseBody
    public ApiResult<?> findForm(@PathVariable(required = false) String id) {
        return this.resultDataOk(this.service.findForm(id));
    }

    /**
     * 删除所有数据
     *
     * @return json
     */
    @ApiOperation("删除所有数据")
    @GetMapping("deleteAllData")
    @ResponseBody
    public ApiResult<?> deleteList() {
        this.service.deleteList();
        return this.resultOk();
    }

}
