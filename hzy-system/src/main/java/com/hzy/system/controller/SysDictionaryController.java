package com.hzy.system.controller;

import com.hzy.infrastructure.annotation.AdminActionDescribe;
import com.hzy.infrastructure.annotation.AdminControllerDescriptor;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import com.hzy.system.domain.SysDictionary;
import com.hzy.system.domain.consts.AdminFunctionConst;
import com.hzy.system.domain.dto.SysDictionaryTreeDto;
import com.hzy.system.service.ISysDictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 数据字典控制器
 *
 * @author HZY
 */
@Api(tags = "数据字典控制器")
@RestController
@RequestMapping(AppConst.SYSTEM_ROUTE_PREFIX + "sysDictionary/")
@AdminControllerDescriptor(23)
public class SysDictionaryController extends ApiBaseController<ISysDictionaryService> {


    public SysDictionaryController(ISysDictionaryService iSysDictionaryService) {
        super(iSysDictionaryService);
    }

    /**
     * 查询列表
     *
     * @param page   页码
     * @param size   页数
     * @param search 查询
     * @return json
     */
    @ApiOperation("查询列表")
    @PostMapping("findList/{size}/{page}")
    @ResponseBody
    @AdminActionDescribe(value = AdminFunctionConst.DISPLAY)
    public ApiResult<?> findList(@PathVariable Integer size, @PathVariable Integer page, @RequestBody SysDictionary search) {
        PagingVo<Map<String, Object>> pagingVo = this.service.findList(page, size, search);
        return this.resultDataOk(pagingVo);
    }

    /**
     * 根据 id 查询表单信息
     *
     * @param id
     * @return json
     */
    @ApiOperation("根据 id 查询表单信息")
    @GetMapping({"findForm/{id}", "findForm"})
    @ResponseBody
    public ApiResult<?> findForm(@PathVariable(required = false) Integer id) {
        return this.resultDataOk(this.service.findForm(id));
    }

    /**
     * 保存数据
     *
     * @param form 要保存数据
     * @return json
     */
    @ApiOperation("保存数据")
    @PostMapping("saveForm")
    @ResponseBody
    public ApiResult<?> saveForm(@RequestBody SysDictionary form) {
        return this.resultDataOk(this.service.saveForm(form));
    }

    /**
     * 删除
     *
     * @param ids id集合
     * @return json
     */
    @ApiOperation("删除")
    @PostMapping("deleteList")
    @ResponseBody
    public ApiResult<?> deleteList(@RequestBody List<Integer> ids) {
        this.service.deleteList(ids);
        return this.resultOk();
    }

    /**
     * 获取字典树
     *
     * @return
     */
    @ApiOperation("获取字典树")
    @GetMapping("getDictionaryTree")
    @ResponseBody
    public ApiResult<List<SysDictionaryTreeDto>> getDictionaryTree() {
        return this.resultDataOk(this.service.getDictionaryTree());
    }

}
