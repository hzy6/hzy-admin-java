package com.hzy.system.controller;

import com.hzy.infrastructure.annotation.AdminControllerDescriptor;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import com.hzy.system.domain.SysUser;
import com.hzy.system.domain.vo.UpdatePasswordVo;
import com.hzy.system.service.IAccountService;
import com.hzy.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * 个人中心
 *
 * @author HZY
 */
@Api(tags = "个人中心")
@RestController
@RequestMapping(AppConst.SYSTEM_ROUTE_PREFIX + "personalCenter/")
@AdminControllerDescriptor(19)
public class PersonalCenterController extends ApiBaseController<IAccountService> {
    private final ISysUserService sysUserService;

    public PersonalCenterController(IAccountService iAccountService, ISysUserService sysUserService) {
        super(iAccountService);
        this.sysUserService = sysUserService;
    }

    /**
     * 修改密码
     *
     * @param form
     * @return
     */
    @ApiOperation("修改密码")
    @PostMapping("changePassword")
    @ResponseBody
    public ApiResult<?> changePassword(@RequestBody UpdatePasswordVo form) {
        this.service.changePassword(form);
        return this.resultOk();
    }

    /**
     * 更新当前登陆人信息
     *
     * @param form
     * @return
     */
    @ApiOperation("更新当前登陆人信息")
    @PostMapping("updateCurrentUser")
    @ResponseBody
    public ApiResult<?> updateCurrentUser(@RequestBody SysUser form) {
        return this.resultDataOk(this.sysUserService.updateCurrentUser(form));
    }

}
