package com.hzy.system.controller;

import com.hzy.infrastructure.annotation.AdminActionDescribe;
import com.hzy.infrastructure.annotation.AdminControllerDescriptor;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import com.hzy.system.domain.SysRole;
import com.hzy.system.domain.consts.AdminFunctionConst;
import com.hzy.system.domain.vo.SysRoleMenuFunctionVo;
import com.hzy.system.service.ISysRoleMenuFunctionService;
import com.hzy.system.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 角色功能控制器
 *
 * @author HZY
 */
@Api(tags = "角色功能控制器")
@RestController
@RequestMapping(AppConst.SYSTEM_ROUTE_PREFIX + "sysRoleMenuFunction/")
@AdminControllerDescriptor(18)
public class SysRoleMenuFunctionController extends ApiBaseController<ISysRoleMenuFunctionService> {
    private final ISysRoleService sysRoleService;

    public SysRoleMenuFunctionController(ISysRoleMenuFunctionService sysRoleMenuFunctionService, ISysRoleService sysRoleService) {
        super(sysRoleMenuFunctionService);
        this.sysRoleService = sysRoleService;
    }

    /**
     * 查询列表
     *
     * @param page   页码
     * @param size   页数
     * @param search 查询
     * @return json
     */
    @ApiOperation("查询列表")
    @PostMapping("findList/{size}/{page}")
    @ResponseBody
    @AdminActionDescribe(value = AdminFunctionConst.DISPLAY)
    public ApiResult<?> findList(@PathVariable Integer size, @PathVariable Integer page, @RequestBody SysRole search) {
        PagingVo<Map<String, Object>> pagingVo = this.sysRoleService.findList(page, size, search);
        return this.resultDataOk(pagingVo);
    }

    /**
     * 保存数据
     *
     * @param form 要保存数据
     * @return json
     */
    @ApiOperation("保存数据")
    @PostMapping("saveForm")
    @ResponseBody
    public ApiResult<?> saveForm(@RequestBody List<SysRoleMenuFunctionVo> form) {
        return this.resultDataOk(this.service.saveForm(form));
    }

    /**
     * 获取菜单功能树
     *
     * @param roleId
     * @return
     */
    @ApiOperation("获取菜单功能树")
    @GetMapping("getRoleMenuFunctionByRoleId/{roleId}")
    @ResponseBody
    public ApiResult<?> getRoleMenuFunctionByRoleId(@PathVariable String roleId) {
        return this.resultDataOk(this.service.getRoleMenuFunctionByRoleId(roleId));
    }

}
