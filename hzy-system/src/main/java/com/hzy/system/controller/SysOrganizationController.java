package com.hzy.system.controller;

import com.hzy.infrastructure.annotation.AdminActionDescribe;
import com.hzy.infrastructure.annotation.AdminControllerDescriptor;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import com.hzy.system.domain.SysOrganization;
import com.hzy.system.domain.consts.AdminFunctionConst;
import com.hzy.system.service.ISysOrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 组织机构控制器
 *
 * @author HZY
 */
@Api(tags = "组织机构控制器")
@RestController
@RequestMapping(AppConst.SYSTEM_ROUTE_PREFIX + "sysOrganization/")
@AdminControllerDescriptor(21)
public class SysOrganizationController extends ApiBaseController<ISysOrganizationService> {

    public SysOrganizationController(ISysOrganizationService iSysOrganizationService) {
        super(iSysOrganizationService);
    }

    /**
     * 查询列表
     *
     * @param search 查询
     * @return json
     */
    @ApiOperation("查询列表")
    @PostMapping("findList/")
    @ResponseBody
    @AdminActionDescribe(value = AdminFunctionConst.DISPLAY)
    public ApiResult<?> findList(@RequestBody SysOrganization search) {
        return this.resultDataOk(this.service.findList(search));
    }

    /**
     * 根据 id 查询表单信息
     *
     * @param id id
     * @return json
     */
    @ApiOperation("根据 id 查询表单信息")
    @GetMapping({"findForm/{id}/{parentId}"})
    @ResponseBody
    public ApiResult<?> findForm(@PathVariable(required = false) Integer id, @PathVariable(required = false) Integer parentId) {
        return this.resultDataOk(this.service.findForm(id, parentId));
    }

    /**
     * 保存数据
     *
     * @param form 要保存数据
     * @return json
     */
    @ApiOperation("保存数据")
    @PostMapping("saveForm")
    @ResponseBody
    public ApiResult<?> saveForm(@RequestBody SysOrganization form) {
        return this.resultDataOk(this.service.saveForm(form));
    }

    /**
     * 删除
     *
     * @param ids id集合
     * @return json
     */
    @ApiOperation("删除")
    @PostMapping("deleteList")
    @ResponseBody
    public ApiResult<?> deleteList(@RequestBody List<Integer> ids) {
        this.service.deleteList(ids);
        return this.resultOk();
    }
}
