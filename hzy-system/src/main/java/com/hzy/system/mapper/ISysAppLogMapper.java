package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysOperationLog;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysAppLogMapper extends BaseMapper<SysOperationLog> {
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysOperationLog search);
}
