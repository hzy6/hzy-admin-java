package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzy.system.domain.SysUserRole;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysUserRoleMapper extends BaseMapper<SysUserRole> {

}
