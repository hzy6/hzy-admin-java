package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzy.system.domain.SysMenu;
import com.hzy.system.domain.SysRoleMenuFunction;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysRoleMenuFunctionMapper extends BaseMapper<SysRoleMenuFunction> {
    List<SysMenu> selectMenuByRoleId(List<String> roleIds);
}
