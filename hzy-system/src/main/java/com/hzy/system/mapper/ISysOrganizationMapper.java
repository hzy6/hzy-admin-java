package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysOrganization;
import com.hzy.system.domain.SysPost;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysOrganizationMapper extends BaseMapper<SysOrganization> {
    /**
     * 获取集合
     *
     * @param search search
     * @return result
     */
    @MapKey("getList")
    List<SysOrganization> getList(@Param("search") SysOrganization search);

    /**
     * 获取最大编号
     *
     * @param parentId parentId
     * @return result
     */
    Integer getMaxNumberByParentId(@Param("parentId") Integer parentId);
}
