package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysDictionary;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysDictionaryMapper extends BaseMapper<SysDictionary> {
    /**
     * @param iPage  分页
     * @param search 查询
     * @return 返回：数据源
     */
    @MapKey("getList")
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysDictionary search);
}
