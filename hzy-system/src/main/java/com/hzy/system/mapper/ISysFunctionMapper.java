package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysFunction;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysFunctionMapper extends BaseMapper<SysFunction> {
    /**
     * @param iPage  分页
     * @param search 查询
     * @return 返回：数据源
     */
    @MapKey("getList")
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysFunction search);

    /**
     * 获取最大编号
     *
     * @return 返回：最大编号
     */
    Integer getMaxNumber();
}
