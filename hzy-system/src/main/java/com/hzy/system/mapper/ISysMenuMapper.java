package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysMenu;
import com.hzy.system.domain.dto.SysMenuTreeDto;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 获取菜单集合 分页查询
     *
     * @param iPage  iPage
     * @param search search
     * @return result
     */
    @MapKey("getList")
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysMenu search);

    /**
     * 获取所有的菜单集合
     *
     * @param search SEARCH
     * @return result
     */
    @MapKey("getAllList")
    List<SysMenuTreeDto> getAllList(@Param("search") SysMenu search);

}
