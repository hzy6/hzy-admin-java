package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysPost;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysPostMapper extends BaseMapper<SysPost> {

    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysPost search);

    /**
     * 获取角色最大编号
     *
     * @return
     */
    Integer getMaxNumber();

}
