package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-26
 */
public interface ISysUserMapper extends BaseMapper<SysUser> {
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysUser search);

    /**
     * 根据登录名称获取用户信息
     *
     * @param loginName 登录名称
     * @return 返回：用户信息
     */
    SysUser getUserByLoginName(@Param("loginName") String loginName);
}
