package com.hzy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.system.domain.SysRole;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysRoleMapper extends BaseMapper<SysRole> {
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, SysRole search);

    /**
     * 获取角色最大编号
     *
     * @return
     */
    Integer getMaxNumber();
}
