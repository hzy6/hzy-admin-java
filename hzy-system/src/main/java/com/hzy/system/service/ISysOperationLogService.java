package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysFunction;
import com.hzy.system.domain.SysOperationLog;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysOperationLogService extends IService<SysOperationLog> {

    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysOperationLog search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(String id);

    /**
     * 日志保存
     *
     * @param sysOperationLog
     */
    void saveLog(SysOperationLog sysOperationLog);

    /**
     * 根据 id 集合 删除 数据
     */
    void deleteList();
}
