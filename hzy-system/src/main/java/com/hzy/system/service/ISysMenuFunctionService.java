package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.system.domain.SysMenuFunction;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysMenuFunctionService extends IService<SysMenuFunction> {

}
