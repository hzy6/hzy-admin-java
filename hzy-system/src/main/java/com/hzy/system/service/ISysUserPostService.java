package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.system.domain.SysUserPost;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysUserPostService extends IService<SysUserPost> {
}
