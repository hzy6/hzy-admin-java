package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysUser;
import com.hzy.system.domain.bo.AccountContext;
import com.hzy.system.domain.dto.SysOrganizationTreeDto;
import com.hzy.system.domain.vo.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-26
 */
public interface ISysUserService extends IService<SysUser> {
    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysUser search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(String id);

    /**
     * 保存
     *
     * @param form
     * @return
     */
    String saveForm(SysUserVo form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<String> ids);

    /**
     * 获取用户和菜单信息
     *
     * @return
     */
    AccountContext getUserAndMenuInfo();

    /**
     * 对部门树 加工树结构
     *
     * @param tree tree
     * @return result
     */
    List<Map<String, Object>> getSysOrganizationTree(List<SysOrganizationTreeDto> tree);

    /**
     * 更新当前用户
     *
     * @param form form
     * @return result
     */
    boolean updateCurrentUser(SysUser form);

}
