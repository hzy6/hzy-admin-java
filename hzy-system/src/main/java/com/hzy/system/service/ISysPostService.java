package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysPost;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysPostService extends IService<SysPost> {
    /**
     * 查询列表
     *
     * @param page   页码
     * @param size   页数
     * @param search 检索条件
     * @return result
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysPost search);

    /**
     * 根据Id查询数据
     *
     * @param id id
     * @return result
     */
    Map<String, Object> findForm(String id);

    /**
     * 保存
     *
     * @param form 表单信息
     * @return result
     */
    String saveForm(SysPost form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids id 集合
     */
    void deleteList(List<String> ids);

}
