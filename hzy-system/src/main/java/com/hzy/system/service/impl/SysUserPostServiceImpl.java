package com.hzy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.system.domain.SysUserPost;
import com.hzy.system.mapper.ISysUserPostMapper;
import com.hzy.system.service.ISysUserPostService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
@Service
public class SysUserPostServiceImpl extends ServiceImpl<ISysUserPostMapper, SysUserPost> implements ISysUserPostService {

}
