package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.system.domain.SysRoleMenuFunction;
import com.hzy.system.domain.vo.SysRoleMenuFunctionTreeVo;
import com.hzy.system.domain.vo.SysRoleMenuFunctionVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
public interface ISysRoleMenuFunctionService extends IService<SysRoleMenuFunction> {
    /**
     * 保存
     *
     * @param form
     * @return
     */
    String saveForm(List<SysRoleMenuFunctionVo> form);

    /**
     * 根据角色id 获取角色菜单功能
     *
     * @param roleId
     * @return
     */
    List<Map<String, Object>> getRoleMenuFunctionByRoleId(String roleId);

}
