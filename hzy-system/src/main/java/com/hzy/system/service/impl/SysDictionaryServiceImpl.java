package com.hzy.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.infrastructure.Tools;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.infrastructure.exception.MessageBox;
import com.hzy.system.domain.SysDictionary;
import com.hzy.system.domain.dto.SysDictionaryTreeDto;
import com.hzy.system.mapper.ISysDictionaryMapper;
import com.hzy.system.service.ISysDictionaryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
@Service
public class SysDictionaryServiceImpl extends ServiceImpl<ISysDictionaryMapper, SysDictionary> implements ISysDictionaryService {
    @Override
    public PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysDictionary search) {
        Page<Map<String, Object>> iPage = new Page<>(page, size);
        List<Map<String, Object>> data = this.baseMapper.getList(iPage, search);
        return PagingVo.page(iPage, data);
    }

    @Override
    public Map<String, Object> findForm(Integer id) {
        Map<String, Object> map = new HashMap<>(10);

        SysDictionary form = this.baseMapper.selectById(id);
        form = Tools.nullSafe(form, new SysDictionary());

        map.put("id", id);
        map.put("form", form);
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer saveForm(SysDictionary form) {
        this.saveOrUpdate(form);
        return form.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(List<Integer> ids) {
        this.removeByIds(ids);
    }

    /**
     * 创建字典树
     *
     * @param id
     * @param allDictionary
     * @return
     */
    private List<SysDictionaryTreeDto> CreateDictionaryTree(Integer id, List<SysDictionary> allDictionary) {
        List<SysDictionaryTreeDto> result = new ArrayList<>();

        List<SysDictionary> data;
        if (id == 0) {
            data = allDictionary.stream().filter(w -> ObjectUtils.isEmpty(w.getParentId()) || w.getParentId() == 0).collect(Collectors.toList());
        } else {
            data = allDictionary.stream().filter(w -> Objects.equals(w.getParentId(), id)).collect(Collectors.toList());
        }

        for (SysDictionary item : data) {
            SysDictionaryTreeDto model = new SysDictionaryTreeDto();
            BeanUtils.copyProperties(item, model);
            model.setKey(model.getId());
            model.setTitle(model.getName());
            model.setChildren(this.CreateDictionaryTree(item.getId(), allDictionary));
            result.add(model);
        }

        return result;
    }

    @Override
    public List<SysDictionaryTreeDto> getDictionaryTree() {
        return this.CreateDictionaryTree(0, this.list());
    }

    @Override
    public List<SysDictionaryTreeDto> getDictionaryByCode(String code) {
        if (ObjectUtils.isEmpty(code)) {
            MessageBox.show("参数Code是空!");
        }

        SysDictionary sysDictionary = this.lambdaQuery().eq(SysDictionary::getCode, code).one();
        if (ObjectUtils.isEmpty(sysDictionary)) {
            return null;
        }
        //检测该字典是否是一个父节点
        List<SysDictionary> sysDictionaries = this.lambdaQuery().eq(SysDictionary::getParentId, sysDictionary.getId()).list();
        if (sysDictionaries != null && sysDictionaries.size() > 0) {
            return null;
        }

        return this.CreateDictionaryTree(sysDictionary.getId(), sysDictionaries);
    }
}
