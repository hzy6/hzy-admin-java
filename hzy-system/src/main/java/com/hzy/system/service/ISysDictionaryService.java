package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysDictionary;
import com.hzy.system.domain.dto.SysDictionaryTreeDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysDictionaryService extends IService<SysDictionary> {
    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysDictionary search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(Integer id);

    /**
     * 保存
     *
     * @param form
     * @return
     */
    Integer saveForm(SysDictionary form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<Integer> ids);

    /**
     * 获取字典树
     *
     * @return
     */
    List<SysDictionaryTreeDto> getDictionaryTree();

    /**
     * 根据 Code 获取 字典集
     *
     * @param code
     * @return
     */
    List<SysDictionaryTreeDto> getDictionaryByCode(String code);

}
