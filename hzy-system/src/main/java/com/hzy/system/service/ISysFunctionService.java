package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysFunction;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysFunctionService extends IService<SysFunction> {
    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysFunction search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(String id);

    /**
     * 保存
     *
     * @param form
     * @return
     */
    String saveForm(SysFunction form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<String> ids);
}
