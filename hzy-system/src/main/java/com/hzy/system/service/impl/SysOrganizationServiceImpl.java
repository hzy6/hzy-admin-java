package com.hzy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.infrastructure.Tools;
import com.hzy.system.domain.SysOrganization;
import com.hzy.system.domain.dto.SysOrganizationTreeDto;
import com.hzy.system.mapper.ISysOrganizationMapper;
import com.hzy.system.service.ISysOrganizationService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
@Service
public class SysOrganizationServiceImpl extends ServiceImpl<ISysOrganizationMapper, SysOrganization> implements ISysOrganizationService {
    @Override
    public List<SysOrganization> findList(SysOrganization search) {
        return this.baseMapper.getList(search);
    }

    @Override
    public Map<String, Object> findForm(Integer id, Integer parentId) {
        Map<String, Object> map = new HashMap<>(2);

        SysOrganization form = this.baseMapper.selectById(id);
        form = Tools.nullSafe(form, new SysOrganization());

        if (ObjectUtils.isEmpty(id) || id == 0) {
            Integer maxNumber = this.baseMapper.getMaxNumberByParentId(parentId);
            form.setOrderNumber((ObjectUtils.isEmpty(maxNumber) ? 0 : maxNumber) + 1);
        }

        map.put("id", id);
        map.put("form", form);
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer saveForm(SysOrganization form) {
        this.saveOrUpdate(form);

        //等级码计算
        if (ObjectUtils.isEmpty(form.getParentId()) || form.getParentId() == 0) {
            form.setLevelCode(form.getId().toString());
        } else {
            SysOrganization parent = this.baseMapper.selectById(form.getParentId());
            form.setLevelCode(parent.getLevelCode() + "." + form.getId());
        }
        this.saveOrUpdate(form);

        return form.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(List<Integer> ids) {
        for (Integer item : ids) {
            //删除当前菜单及一下的子集菜单
            SysOrganization sysOrganization = this.baseMapper.selectById(item);
            this.lambdaUpdate().eq(SysOrganization::getLevelCode, sysOrganization.getLevelCode()).or().likeLeft(SysOrganization::getLevelCode, sysOrganization.getLevelCode() + ".").remove();
        }

    }

    @Override
    public List<SysOrganizationTreeDto> getSysOrganizationTree(List<Integer> expandedRowKeys, List<SysOrganization> sysOrganizations, SysOrganization sysOrganization) {
        List<SysOrganizationTreeDto> result = new ArrayList<>();
        boolean isFirst = (sysOrganizations == null || sysOrganizations.size() == 0) && sysOrganization == null;
        //如果是第一次调用
        if (isFirst) {
            sysOrganizations = this.lambdaQuery().orderByAsc(SysOrganization::getOrderNumber).list();
            List<SysOrganization> sysOrganizationList = sysOrganizations.stream().filter(w -> ObjectUtils.isEmpty(w.getParentId()) || w.getParentId() == 0).collect(Collectors.toList());
            for (SysOrganization item : sysOrganizationList) {
                SysOrganizationTreeDto sysOrganizationTreeDto = new SysOrganizationTreeDto();
                BeanUtils.copyProperties(item, sysOrganizationTreeDto);
                sysOrganizationTreeDto.setChildren(this.getSysOrganizationTree(expandedRowKeys, sysOrganizations, item));
                result.add(sysOrganizationTreeDto);
            }
        } else {
            if (sysOrganization != null && sysOrganizations != null) {
                expandedRowKeys.add(sysOrganization.getId());
                List<SysOrganization> list = sysOrganizations.stream().filter(w -> sysOrganization.getId().equals(w.getParentId())).collect(Collectors.toList());
                for (SysOrganization item : list) {
                    SysOrganizationTreeDto sysOrganizationTreeDto = new SysOrganizationTreeDto();
                    BeanUtils.copyProperties(item, sysOrganizationTreeDto);
                    sysOrganizationTreeDto.setChildren(this.getSysOrganizationTree(expandedRowKeys, sysOrganizations, item));
                    result.add(sysOrganizationTreeDto);
                }
            }
        }

        return result;
    }
}
