package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysMenu;
import com.hzy.system.domain.dto.SysMenuTreeDto;
import com.hzy.system.domain.vo.SysMenuVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysMenu search);

    /**
     * 查询所有的菜单集合
     *
     * @param search search
     * @return result
     */
    List<SysMenuTreeDto> getAll(SysMenu search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(Integer id);

    /**
     * 保存
     *
     * @param form
     * @return
     */
    Integer saveForm(SysMenuVo form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<Integer> ids);

    /**
     * 创建菜单树
     *
     * @param id
     * @param sysMenuList
     * @return
     */
    List<SysMenuTreeDto> createMenus(Integer id, List<SysMenu> sysMenuList);

    /**
     * 根据角色 id 获取 菜单
     *
     * @return
     */
    List<SysMenu> getMenusByCurrentRole();

    /**
     * 根据菜单 id 获取 权限
     *
     * @param id
     * @return
     */
    Map<String, Boolean> getPowerStateByMenuId(Integer id);

    /**
     * 根据菜单 当前用户拥有得 菜单 获取他们得 权限集合
     *
     * @param menus
     * @return
     */
    List<Map<String, Object>> getPowerByMenus(List<SysMenu> menus);

}
