package com.hzy.system.service;

import com.hzy.system.domain.bo.AccountContext;
import com.hzy.system.domain.vo.AuthUserVo;
import com.hzy.system.domain.vo.UpdatePasswordVo;

/**
 * 账户服务
 *
 * @author hzy
 */
public interface IAccountService {

    /**
     * 获取 token 字符串
     *
     * @return
     */
    String getToken();

    /**
     * 获取账户信息
     *
     * @return accountContext
     */
    AccountContext getAccountContext();

    /**
     * 设置 账户信息
     *
     * @param accountInfo
     */
    void setAccountContext(AccountContext accountContext);

    /**
     * 检查账户密码信息
     *
     * @param authUserVo
     * @return
     */
    String checkAccountAsync(AuthUserVo authUserVo);

    /**
     * 修改密码
     *
     * @param updatePasswordVo
     * @return
     */
    Integer changePassword(UpdatePasswordVo updatePasswordVo);


}
