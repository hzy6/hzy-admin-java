package com.hzy.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.infrastructure.SpringUtils;
import com.hzy.infrastructure.Tools;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.system.domain.SysOperationLog;
import com.hzy.system.domain.SysUser;
import com.hzy.system.domain.bo.AccountContext;
import com.hzy.system.mapper.ISysOperationLogMapper;
import com.hzy.system.service.IAccountService;
import com.hzy.system.service.ISysOperationLogService;
import com.hzy.system.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
@Service
public class SysOperationLogServiceImpl extends ServiceImpl<ISysOperationLogMapper, SysOperationLog> implements ISysOperationLogService {

    private final IAccountService accountService;
    private final ISysUserService sysUserService;

    public SysOperationLogServiceImpl(IAccountService accountService, ISysUserService sysUserService) {
        this.accountService = accountService;
        this.sysUserService = sysUserService;
    }

    @Override
    public PagingVo<Map<String, Object>> findList(Integer page, Integer size, SysOperationLog search) {
        IPage<Map<String, Object>> iPage = new Page<>(page, size);
        List<Map<String, Object>> data = this.baseMapper.getList(iPage, search);
        return PagingVo.page(iPage, data);
    }

    @Override
    public Map<String, Object> findForm(String id) {
        Map<String, Object> map = new HashMap<>(10);

        SysOperationLog form = this.baseMapper.selectById(id);
        form = Tools.nullSafe(form, new SysOperationLog());

        SysUser sysUser = this.sysUserService.getById(form.getUserId());
        sysUser = Tools.nullSafe(sysUser, new SysUser());

        map.put("id", id);
        map.put("form", form);
        map.put("user", sysUser);
        return map;
    }

    @Override
    public void saveLog(SysOperationLog sysOperationLog) {
        AccountContext accountInfo = accountService.getAccountContext();
        if (!ObjectUtils.isEmpty(accountInfo)) {
            sysOperationLog.setUserId(accountInfo.getId());
            sysOperationLog.setLastModifierUserId(accountInfo.getId());
            sysOperationLog.setCreatorUserId(accountInfo.getId());
        }

        //开启子线程共享请求服务
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        RequestContextHolder.setRequestAttributes(servletRequestAttributes, true);

        //开启线程中保存信息
        new Thread(() -> {
            ISysOperationLogMapper sysOperationLogMapper = SpringUtils.getBean(ISysOperationLogMapper.class);
            sysOperationLogMapper.insert(sysOperationLog);
        }).start();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList() {
        this.remove(new LambdaQueryWrapper<>());
    }

}
