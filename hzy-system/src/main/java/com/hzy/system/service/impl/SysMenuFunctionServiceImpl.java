package com.hzy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.system.domain.SysMenuFunction;
import com.hzy.system.mapper.ISysMenuFunctionMapper;
import com.hzy.system.service.ISysMenuFunctionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hzy
 * @since 2020-04-29
 */
@Service
public class SysMenuFunctionServiceImpl extends ServiceImpl<ISysMenuFunctionMapper, SysMenuFunction> implements ISysMenuFunctionService {

}
