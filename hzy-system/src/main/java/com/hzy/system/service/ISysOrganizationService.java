package com.hzy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.system.domain.SysOrganization;
import com.hzy.system.domain.dto.SysOrganizationTreeDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hzy
 * @since 2020-04-28
 */
public interface ISysOrganizationService extends IService<SysOrganization> {
    /**
     * 查询列表
     *
     * @param search 检索条件
     * @return result
     */
    List<SysOrganization> findList(SysOrganization search);

    /**
     * 根据Id查询数据
     *
     * @param id       id
     * @param parentId parentId
     * @return result
     */
    Map<String, Object> findForm(Integer id, Integer parentId);

    /**
     * 保存
     *
     * @param form 表单信息
     * @return result
     */
    Integer saveForm(SysOrganization form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids id 集合
     */
    void deleteList(List<Integer> ids);

    /**
     * 获取组织机构树
     *
     * @param expandedRowKeys  expandedRowKeys
     * @param sysOrganizations sysOrganizations
     * @param sysOrganization  sysOrganization
     * @return result
     */
    List<SysOrganizationTreeDto> getSysOrganizationTree(List<Integer> expandedRowKeys, List<SysOrganization> sysOrganizations, SysOrganization sysOrganization);

}
