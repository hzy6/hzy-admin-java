package com.hzy.generation.controller;

import com.hzy.generation.domain.SysTableSchema;
import com.hzy.generation.domain.dto.GenForm;
import com.hzy.generation.service.IGenService;
import com.hzy.generation.service.ISysTableSchemaService;
import com.hzy.infrastructure.domain.consts.AppConst;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.infrastructure.exception.ApiResult;
import com.hzy.infrastructure.web.controller.ApiBaseController;
import freemarker.template.TemplateException;
import org.springframework.web.bind.annotation.*;

import java.io.*;

/**
 * 代码生成控制器
 */
@RestController
@RequestMapping(AppConst.ADMIN_ROUTE_PREFIX + "gen/")
public class GenController extends ApiBaseController<IGenService> {
    private final ISysTableSchemaService sysTableSchemaService;

    public GenController(IGenService genService, ISysTableSchemaService sysTableSchemaService) {
        super(genService);
        this.sysTableSchemaService = sysTableSchemaService;
    }

    /**
     * 获取数据库中所有的表
     *
     * @param size
     * @param page
     * @param search
     * @return
     */
    @ResponseBody
    @PostMapping("findList/{size}/{page}")
    public ApiResult<PagingVo<SysTableSchema>> getTables(@PathVariable Integer size, @PathVariable Integer page, @RequestBody SysTableSchema search) {
        return this.resultDataOk(sysTableSchemaService.findList(page, size, search.getName()));
    }

    /**
     * 生成 domain
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("domain")
    public ApiResult<String> createDomain(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genDomain(form));
    }

    /**
     * 生成 mapper
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("mapper")
    public ApiResult<String> createMapper(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genMapper(form));
    }

    /**
     * 生成 mapperXml
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("mapperXml")
    public ApiResult<String> createMapperXml(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genMapperXml(form));
    }

    /**
     * 生成 service
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("service")
    public ApiResult<String> createService(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genService(form));
    }

    /**
     * 生成 service impl
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("serviceImpl")
    public ApiResult<String> createServiceImpl(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genServiceImpl(form));
    }

    /**
     * 生成 controller
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("controller")
    public ApiResult<String> createController(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genController(form));
    }

    /**
     * 生成 service js
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("serviceJs")
    public ApiResult<String> createServiceJs(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genServiceJs(form));
    }

    /**
     * 生成 index vue
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("indexVue")
    public ApiResult<String> createIndexVue(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genIndexVue(form));
    }

    /**
     * 生成 info vue
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    @ResponseBody
    @PostMapping("infoVue")
    public ApiResult<String> createInfoVue(@RequestBody GenForm form) throws IOException, TemplateException {
        return this.resultDataOk(this.service.genInfoVue(form));
    }

}
