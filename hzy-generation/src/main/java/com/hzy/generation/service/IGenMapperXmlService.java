package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenForm;
import com.hzy.generation.domain.dto.MapperXmlGenContext;

/**
 * mapper xml 服务类
 */
public interface IGenMapperXmlService {
    MapperXmlGenContext getGenContext(GenForm form);
}
