package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * service js 服务类
 */
public interface IGenServiceJsService {
    GenContext getGenContext(GenForm form);
}
