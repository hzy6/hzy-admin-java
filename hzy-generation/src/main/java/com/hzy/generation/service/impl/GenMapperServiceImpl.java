package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenMapperService;
import org.springframework.stereotype.Service;

/**
 * mapper 服务类
 */
@Service
public class GenMapperServiceImpl extends GenBaseServiceImpl implements IGenMapperService {

    private final ISysTableSchemaMapper sysTableSchemaMapper;
    private final ISysTableInfoMapper sysTableInfoMapper;

    public GenMapperServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
        this.sysTableSchemaMapper = sysTableSchemaMapper;
        this.sysTableInfoMapper = sysTableInfoMapper;
    }
}
