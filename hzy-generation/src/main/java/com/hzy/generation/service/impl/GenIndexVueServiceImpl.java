package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenIndexVueService;
import org.springframework.stereotype.Service;

/**
 * domain 服务类
 */
@Service
public class GenIndexVueServiceImpl extends GenBaseServiceImpl implements IGenIndexVueService {
    public GenIndexVueServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
    }
}
