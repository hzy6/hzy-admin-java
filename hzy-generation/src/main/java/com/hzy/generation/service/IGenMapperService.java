package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * mapper 服务类
 */
public interface IGenMapperService {
    GenContext getGenContext(GenForm form);
}
