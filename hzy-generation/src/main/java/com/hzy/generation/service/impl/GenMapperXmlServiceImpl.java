package com.hzy.generation.service.impl;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;
import com.hzy.generation.domain.dto.MapperXmlGenContext;
import com.hzy.generation.domain.dto.SysTableInfoDto;
import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenMapperXmlService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * mapper 服务类
 */
@Service
public class GenMapperXmlServiceImpl extends GenBaseServiceImpl implements IGenMapperXmlService {

    public GenMapperXmlServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
    }

    @Override
    public MapperXmlGenContext getGenContext(GenForm form) {
        GenContext genContext = super.getGenContext(form);
        MapperXmlGenContext mapperXmlGenContext = new MapperXmlGenContext();
        BeanUtils.copyProperties(genContext, mapperXmlGenContext);
        String whereSql = "";
        for (SysTableInfoDto sysTableInfoDto : mapperXmlGenContext.getSysTableInfoDtoList()) {
            whereSql +=
                    "       <if test=\"search." + sysTableInfoDto.getName() + "!=null and search." + sysTableInfoDto.getName() + "!=''\">\n" +
                            "        and " + sysTableInfoDto.getName() + " like concat('%',#{search." + sysTableInfoDto.getName() + "}, '%')\n" +
                            "       </if>\n";
        }

        mapperXmlGenContext.setWhereSqlString(whereSql);

        return mapperXmlGenContext;
    }
}
