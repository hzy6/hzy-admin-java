package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * service Impl 服务类
 */
public interface IGenServiceImplService {
    GenContext getGenContext(GenForm form);
}
