package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenForm;
import freemarker.template.TemplateException;

import java.io.IOException;

public interface IGenService {

    /**
     * 生成 domain
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genDomain(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 mapper
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genMapper(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 mapperXml
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genMapperXml(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 service
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genService(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 service impl
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genServiceImpl(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 controller
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genController(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 service js
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genServiceJs(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 index vue
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genIndexVue(GenForm form) throws IOException, TemplateException;

    /**
     * 生成 info vue
     *
     * @param form
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    String genInfoVue(GenForm form) throws IOException, TemplateException;

}
