package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * domain 服务类
 */
public interface IGenDomainService {

    GenContext getGenContext(GenForm form);

}
