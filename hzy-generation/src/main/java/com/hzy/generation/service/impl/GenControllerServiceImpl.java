package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenControllerService;
import org.springframework.stereotype.Service;

/**
 * controller 服务类
 */
@Service
public class GenControllerServiceImpl extends GenBaseServiceImpl implements IGenControllerService {
    public GenControllerServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
    }
}
