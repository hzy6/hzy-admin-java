package com.hzy.generation.service.impl;

import com.hzy.generation.domain.SysTableInfo;
import com.hzy.generation.domain.SysTableSchema;
import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;
import com.hzy.generation.domain.dto.SysTableInfoDto;
import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.infrastructure.Tools;
import org.springframework.beans.BeanUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 代码生成器服务基础代码
 */
public class GenBaseServiceImpl {

    private final ISysTableSchemaMapper sysTableSchemaMapper;
    private final ISysTableInfoMapper sysTableInfoMapper;

    public GenBaseServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        this.sysTableSchemaMapper = sysTableSchemaMapper;
        this.sysTableInfoMapper = sysTableInfoMapper;
    }

    /**
     * 根据数据库类型获取java类型
     *
     * @param dbType
     * @return
     */
    protected String getJavaTypeByDbType(String dbType) {
        dbType = dbType.toLowerCase(Locale.ROOT);
        if ("longtext".equals(dbType)) {
            return "String";
        }

        if ("varchar".equals(dbType)) {
            return "String";
        }

        if ("char".equals(dbType)) {
            return "String";
        }

        if ("bigint".equals(dbType)) {
            return "Long";
        }

        if ("int".equals(dbType)) {
            return "Integer";
        }

        if ("tinyint".equals(dbType)) {
            return "Boolean";
        }

        if ("bit".equals(dbType)) {
            return "Boolean";
        }

        if ("timestamp".equals(dbType)) {
            return "LocalDateTime";
        }

        if ("datetime".equals(dbType)) {
            return "LocalDateTime";
        }

        if ("date".equals(dbType)) {
            return "Date";
        }

        if ("double".equals(dbType)) {
            return "Double";
        }

        if ("decimal".equals(dbType)) {
            return "BigDecimal";
        }

        if ("float".equals(dbType)) {
            return "Float";
        }

        return "String";
    }

    /**
     * 根据生成的表单信息 组装 生成代码需要的上下文信息
     *
     * @param form
     * @return
     */
    public GenContext getGenContext(GenForm form) {
        String tableName = form.getTableName();
        // 获取表信息
        List<SysTableSchema> sysTableSchemas = sysTableSchemaMapper.getTables();
        SysTableSchema sysTableSchema = sysTableSchemas.stream()
                .filter(w -> tableName.equals(w.getName()))
                .findFirst()
                .orElse(new SysTableSchema());

        //获取表字段集合
        List<SysTableInfo> sysTableInfoList = sysTableInfoMapper.getTableInfoByTableName(tableName);
        GenContext domainContext = new GenContext();
        domainContext.setTableName(Tools.getUpperCamelCase(sysTableSchema.getName()));
        domainContext.setTableNameLower(Tools.getLowerCamelCase(sysTableSchema.getName()));
        domainContext.setTableComment(sysTableSchema.getComment());
        domainContext.setCreateTime(LocalDateTime.now());
        //字段类型转换
        List<SysTableInfoDto> sysTableInfoDtoList = new ArrayList<>();
        for (SysTableInfo item : sysTableInfoList) {
            SysTableInfoDto sysTableInfoDto = new SysTableInfoDto();
            BeanUtils.copyProperties(item, sysTableInfoDto);
            //字段类型 数据库类型 转换为 java 类型
            String javaType = this.getJavaTypeByDbType(item.getType());
            sysTableInfoDto.setJavaType(javaType);
            sysTableInfoDtoList.add(sysTableInfoDto);
            //import java.math.BigDecimal;
        }
        domainContext.setSysTableInfoDtoList(sysTableInfoDtoList);
        domainContext.setKeyJavaType(sysTableInfoDtoList.stream().filter(w -> w.isKey()).map(w -> w.getJavaType()).findFirst().orElse(""));
        domainContext.setPackageInfo(form.getPackageInfo());

        return domainContext;
    }


}
