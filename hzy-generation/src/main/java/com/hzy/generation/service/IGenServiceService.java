package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * service 服务类
 */
public interface IGenServiceService {
    GenContext getGenContext(GenForm form);
}
