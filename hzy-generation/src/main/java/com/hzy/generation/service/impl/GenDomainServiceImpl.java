package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenDomainService;
import org.springframework.stereotype.Service;

/**
 * domain 服务类
 */
@Service
public class GenDomainServiceImpl extends GenBaseServiceImpl implements IGenDomainService {

    private final ISysTableSchemaMapper sysTableSchemaMapper;
    private final ISysTableInfoMapper sysTableInfoMapper;

    public GenDomainServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
        this.sysTableSchemaMapper = sysTableSchemaMapper;
        this.sysTableInfoMapper = sysTableInfoMapper;
    }


}
