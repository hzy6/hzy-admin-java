package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenInfoVueService;
import org.springframework.stereotype.Service;

/**
 * info vue 服务类
 */
@Service
public class GenInfoVueServiceImpl extends GenBaseServiceImpl implements IGenInfoVueService {
    public GenInfoVueServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
    }
}
