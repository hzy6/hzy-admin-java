package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * info vue 服务类
 */
public interface IGenInfoVueService {
    GenContext getGenContext(GenForm form);
}
