package com.hzy.generation.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.generation.domain.SysTableSchema;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.ISysTableSchemaService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysTableSchemaServiceImpl extends ServiceImpl<ISysTableSchemaMapper, SysTableSchema> implements ISysTableSchemaService {

    @Override
    public PagingVo<SysTableSchema> findList(Integer page, Integer size, String tableName) {
        Page<SysTableSchema> iPage = new Page<>(page, size);
        List<SysTableSchema> data = this.baseMapper.getTables(iPage, tableName);
        return PagingVo.page(iPage, data);
    }
}
