package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * controller 服务类
 */
public interface IGenControllerService {
    GenContext getGenContext(GenForm form);
}
