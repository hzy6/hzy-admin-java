package com.hzy.generation.service;

import com.hzy.generation.domain.dto.GenContext;
import com.hzy.generation.domain.dto.GenForm;

/**
 * index vue 服务类
 */
public interface IGenIndexVueService {
    GenContext getGenContext(GenForm form);
}
