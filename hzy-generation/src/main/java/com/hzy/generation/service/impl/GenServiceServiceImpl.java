package com.hzy.generation.service.impl;

import com.hzy.generation.mapper.ISysTableInfoMapper;
import com.hzy.generation.mapper.ISysTableSchemaMapper;
import com.hzy.generation.service.IGenServiceService;
import org.springframework.stereotype.Service;

/**
 * service 服务类
 */
@Service
public class GenServiceServiceImpl  extends GenBaseServiceImpl implements IGenServiceService {

    public GenServiceServiceImpl(ISysTableSchemaMapper sysTableSchemaMapper, ISysTableInfoMapper sysTableInfoMapper) {
        super(sysTableSchemaMapper, sysTableInfoMapper);
    }
}
