package com.hzy.generation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.generation.domain.SysTableSchema;
import com.hzy.infrastructure.domain.vo.PagingVo;

public interface ISysTableSchemaService extends IService<SysTableSchema> {

    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param tableName
     * @return
     */
    PagingVo<SysTableSchema> findList(Integer page, Integer size, String tableName);
}
