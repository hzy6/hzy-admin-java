package com.hzy.generation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzy.generation.domain.SysTableSchema;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ISysTableSchemaMapper extends BaseMapper<SysTableSchema> {

    /**
     * 获取所有的表名称
     *
     * @return
     */
    List<SysTableSchema> getTables(IPage<SysTableSchema> iPage, @Param("tableName") String tableName);

    List<SysTableSchema> getTables();

}
