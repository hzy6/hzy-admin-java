package com.hzy.generation.mapper;

import com.hzy.generation.domain.SysTableInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ISysTableInfoMapper {

    /**
     * 获取表对应的 信息
     *
     * @param tableName
     * @return
     */
    List<SysTableInfo> getTableInfoByTableName(@Param("tableName") String tableName);
}
