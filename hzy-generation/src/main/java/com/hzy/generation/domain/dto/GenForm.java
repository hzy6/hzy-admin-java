package com.hzy.generation.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GenForm {
    /**
     * 表名称
     */
    private String tableName;
    /**
     * 包信息
     */
    private String packageInfo;
}
