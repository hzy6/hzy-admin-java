package com.hzy.generation.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * domain dto
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MapperXmlGenContext extends GenContext{
    private String whereSqlString;
}
