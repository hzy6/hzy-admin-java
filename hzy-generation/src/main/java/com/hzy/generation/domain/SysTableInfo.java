package com.hzy.generation.domain;

import com.baomidou.mybatisplus.annotation.TableField;

/**
 * 表信息
 *
 * @author HZY
 */
public class SysTableInfo {
    /**
     * 表名
     */
	@TableField("tableName")
    private String tableName;
	/**
	 * 列名称
	 */
	private String name;
    /**
     * 主键标记
     */
    private String key;
    /**
     * 类型
     */
    private String type;
    /**
     * 长度
     */
    private Long length;
    /**
     * 注解
     */
    private String comment;

    /**
     * 是否为主键
     *
     * @return
     */
    public Boolean isKey() {
        return "PRI".equals(this.key);
    }

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
