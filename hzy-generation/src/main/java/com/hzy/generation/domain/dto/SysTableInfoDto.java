package com.hzy.generation.domain.dto;

import com.hzy.generation.domain.SysTableInfo;

/**
 * SysTableInfoDto
 *
 * @author hzy
 */
public class SysTableInfoDto extends SysTableInfo {

    private String javaType;

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

}
