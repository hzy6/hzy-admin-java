package com.hzy.generation.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * domain dto
 * </p>
 *
 * @author HZY
 * @since 2020-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GenContext {
    private String tableName;
    private String tableNameLower;
    private String tableComment;
    private LocalDateTime createTime;
    private List<SysTableInfoDto> sysTableInfoDtoList;
    /**
     * 主键 java 类型
     */
    private String keyJavaType;
    /**
     * 包信息
     */
    private String packageInfo;
    /**
     * 导入包集合
     */
    private List<String> importList;

}
