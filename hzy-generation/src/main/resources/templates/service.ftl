package ${packageInfo};

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.admin.domain.${tableName};

import java.util.List;
import java.util.Map;

/**
 * <p>
 * ${tableName} 服务类
 * </p>
 *
 * @author hzy
 * @since ${createTime}
 */
public interface I${tableName}Service extends IService<${tableName}> {
    /**
     * 查询列表
     *
     * @param page
     * @param size
     * @param search
     * @return
     */
    PagingVo<Map<String, Object>> findList(Integer page, Integer size, ${tableName} search);

    /**
     * 根据Id查询数据
     *
     * @param id
     * @return
     */
    Map<String, Object> findForm(${keyJavaType} id);

    /**
     * 保存
     *
     * @param form
     * @return
     */
    ${keyJavaType} saveForm(${tableName} form);

    /**
     * 根据 id 集合 删除 数据
     *
     * @param ids
     */
    void deleteList(List<${keyJavaType}> ids);
}
