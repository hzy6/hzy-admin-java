<template>
  <a-modal v-model:visible="visible" title="编辑" centered @ok="visible = false" :width="800">
    <template #footer>
      <a-button type="primary" @click="saveForm()" :loading="saveLoading">提交</a-button>
      <a-button type="danger" ghost @click="visible = false" class="ml-24">关闭</a-button>
    </template>
    <a-spin :spinning="saveLoading">
      <a-form layout="vertical" :model="vm.form">
        <a-row :gutter="[24, 24]">

<#list sysTableInfoDtoList as item>
    <#if !item.isKey()>
        <#if item.comment?? && item.comment!=''>
            <a-col :xs="24" :sm="12" :md="12" :lg="12" :xl="12">
                <a-form-item label="${item.comment}" ref="${item.name}" name="${item.name}">
                  <a-input v-model:value="vm.form.${item.name}" placeholder="请输入" />
                </a-form-item>
            </a-col>
        <#else>
            <a-col :xs="24" :sm="12" :md="12" :lg="12" :xl="12">
                <a-form-item label="${item.name}" ref="${item.name}" name="${item.name}">
                  <a-input v-model:value="vm.form.${item.name}" placeholder="请输入" />
                </a-form-item>
            </a-col>
        </#if>
    </#if>
</#list>

        </a-row>
      </a-form>
    </a-spin>
  </a-modal>
</template>
<script>
import { defineComponent, reactive, toRefs } from "vue";
import tools from "@/scripts/tools";
import service from "@/service/system/${tableNameLower}Service";

export default defineComponent({
  name: "${tableNameLower}_info",
  props: {
    onSuccess: Function,
  },
  setup(props, context) {
    const state = reactive({
      vm: {
        id: "",
        form: {},
      },
      visible: false,
      saveLoading: false,
    });

    const methods = {
      findForm() {
        state.saveLoading = true;
        service.findForm(state.vm.id).then((res) => {
          state.saveLoading = false;
          if (res.code != 1) return;
          state.vm = res.data;
        });
      },
      saveForm() {
        state.saveLoading = true;
        service.saveForm(state.vm.form).then((res) => {
          state.saveLoading = false;
          if (res.code != 1) return;
          tools.message("操作成功!", "成功");
          context.emit("onSuccess", res.data);
          state.visible = false;
        });
      },
      //打开表单初始化
      openForm({ visible, key }) {
        state.visible = visible;
        if (visible) {
          state.vm.id = key;
          methods.findForm();
        }
      },
    };

    context.expose({ ...methods });

    return {
      ...toRefs(state),
      ...methods,
    };
  },
});
</script>
<style lang="less" scoped>
.ant-form-item {
  margin-bottom: 0;
}
</style>
