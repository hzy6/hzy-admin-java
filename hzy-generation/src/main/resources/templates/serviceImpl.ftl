package ${packageInfo};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzy.infrastructure.Tools;
import com.hzy.infrastructure.domain.vo.PagingVo;
import com.hzy.admin.domain.${tableName};
import com.hzy.admin.mapper.I${tableName}Mapper;
import com.hzy.admin.service.I${tableName}Service;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * ${tableName} 服务实现类
 * </p>
 *
 * @author hzy
 * @since ${createTime}
 */
@Service
public class ${tableName}ServiceImpl extends ServiceImpl<I${tableName}Mapper, ${tableName}> implements I${tableName}Service {
    @Override
    public PagingVo<Map<String, Object>> findList(Integer page, Integer size, ${tableName} search) {
        Page<Map<String, Object>> iPage = new Page<>(page, size);
        List<Map<String, Object>> data = this.baseMapper.getList(iPage, search);
        return PagingVo.page(iPage, data);
    }

    @Override
    public Map<String, Object> findForm(${keyJavaType} id) {
        Map<String, Object> map = new HashMap<>(10);

        ${tableName} form = this.baseMapper.selectById(id);
        form = Tools.nullSafe(form, new ${tableName}());

        map.put("id", id);
        map.put("form", form);
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ${keyJavaType} saveForm(${tableName} form) {
        this.saveOrUpdate(form);
        return form.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(List<${keyJavaType}> ids) {
        this.removeByIds(ids);

    }
}
