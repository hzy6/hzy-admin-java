<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.hzy.admin.mapper.I${tableName}Mapper">
    <select id="getList" parameterType="com.hzy.admin.domain.${tableName}" resultType="java.util.Map">
        select * from ${tableName} where 1=1
${whereSqlString}
        order by 1
    </select>
</mapper>
