package ${packageInfo};

import com.baomidou.mybatisplus.annotation.TableField;
import com.hzy.infrastructure.domain.AppBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

<#if importList??>
<#list importList as item>
import ${item};
</#list>
</#if>

/**
 * ${tableName} | ${tableComment}
 * @author HZY
 * @since ${createTime}
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ${tableName} extends AppBaseEntity<${keyJavaType}> {

    private static final long serialVersionUID = 1L;

<#list sysTableInfoDtoList as item>
    <#if (!item.isKey())>
    /**
     * ${item.name}
     <#if (item.comment??)>* ${item.comment}</#if>
     */
    @TableField("${item.name}")
    private ${item.javaType} ${item.name};
    </#if>
</#list>

}