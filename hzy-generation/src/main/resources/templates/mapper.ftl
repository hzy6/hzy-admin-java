package ${packageInfo};

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * ${tableName} Mapper 接口
 * </p>
 *
 * @author hzy
 * @since ${createTime}
 */
public interface I${tableName}Mapper extends BaseMapper<${tableName}> {
    /**
     * ${tableName} list
     * @param iPage  分页
     * @param search 查询
     * @return 返回：数据源
     */
    @MapKey("getList")
    List<Map<String, Object>> getList(IPage<Map<String, Object>> iPage, ${tableName} search);

}
