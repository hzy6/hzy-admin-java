# hzy-admin-spa

#### :yellow_heart: 介绍 
前后分离,后台通配权限管理系统！

### 后台模板
https://gitee.com/hzy6/hzy-admin-spa-ui

### 软件架构
开发环境：Idea 2020.1+ | Java 8

使用到技术：Spring boot | Mybatis Plus | Mysql8.0 | Vue3.0 | Antd Of Vue3.0

前端：Vue3.x 、Antd Of Vue 3.x

数据库脚本位置：根目录/doc/hzy-admin-spa.sql


###  **_交流群: 534584927_** 

#### :cherries:  部分界面截图
| ![输入图片说明](doc/images/image1.png) | ![输入图片说明](doc/images/image2.png) |
|-----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| ![输入图片说明](doc/images/image3.png) | ![输入图片说明](doc/images/image4.png) |
| ![输入图片说明](doc/images/image5.png) | ![输入图片说明](doc/images/image6.png) |
| ![输入图片说明](doc/images/image7.png) | ![输入图片说明](doc/images/image8.png) |

![输入图片说明](doc/images/image9.png)
![输入图片说明](doc/images/image10.png)
![输入图片说明](doc/images/image11.png)

#### 安装前提

1、安装 nodejs

2、安装 vue cli >> cmd 执行: npm install -g @vue/cli

#### 安装教程

1. 前端 UI 在项目跟目录下 hzy-admin-clientapp 使用 VS Code 打开
2. VS Code 打开终端执行CMD命令>> cnpm install 拉包 （node 环境 这些不懂得自行百度查询资料！）
4、命令 npm run build 打包后使用 iis 或者 nginx 部署前端 ui


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
